package com.flow.module.process.listener.applysimple;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flow.module.sys.service.FlowUserService;
import com.system.comm.utils.FrameSpringBeanUtil;
/**
 * 领导审核监听器
 * @author anxpp.com
 * 2016年12月24日 下午12:10:01
 */
public class ApplySimpleNode1Listener implements TaskListener {
	
	private static final long serialVersionUID = 4285398130708457006L;
	private final static Logger LOGGER = LoggerFactory.getLogger(ApplySimpleNode1Listener.class);
	
	@Override
	public void notify(DelegateTask task) {
		//获取设置的参数
		String checkType = task.getVariable("checkType", String.class);
		
		LOGGER.info("节点1审核监听器 - 来源类型[" + checkType + "]...");
		
		//设置任务处理候选人, 只要一个人审批通过即可
		FlowUserService flowUserService = FrameSpringBeanUtil.getBean(FlowUserService.class);
		List<String> candidateUsers = new ArrayList<String>();
		candidateUsers.add(flowUserService.getId("test", "2"));
		//candidateUsers.add(flowUserService.getId("test", "3"));
		task.addCandidateUsers(candidateUsers);
		
		//设置节点2的会签人员的信息
		List<String> joinUsers = new ArrayList<String>();
		joinUsers.add(flowUserService.getId("test", "3"));
		joinUsers.add(flowUserService.getId("test", "4"));
		//task.addCandidateUsers(candidateUsers);
		//设置下个节点的会签信息
		task.setVariable("joinUsers", joinUsers);
	}
}
