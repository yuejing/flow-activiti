package com.flow.module.process.listener.applysimple;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flow.module.process.service.BizApplySimpleService;
import com.system.comm.utils.FrameSpringBeanUtil;
/**
 * 流程监听器
 * @author anxpp.com
 * 2016年12月24日 下午12:33:58
 */
public class ApplySimpleEndListener implements ExecutionListener {
	
	private static final long serialVersionUID = 5212042435691138021L;
	private final static Logger LOGGER = LoggerFactory.getLogger(ApplySimpleEndListener.class);
	
	@Override
	public void notify(DelegateExecution arg0) throws Exception {
		String checkType = arg0.getVariable("checkType", String.class);
		//fail 失败结束
		LOGGER.info("流程结束监听器 - 来源类型[" + checkType + "]...");
		//获取设置的参数
		String id = arg0.getVariable("id").toString();
		//修改业务数据状态
		BizApplySimpleService bizApplySimpleService = FrameSpringBeanUtil.getBean(BizApplySimpleService.class);
		bizApplySimpleService.updateStatus(id, 20);
	}
}
