package com.flow.module.sys.service;

import java.util.List;
import java.util.Map;

import org.activiti.engine.task.Task;

import com.flow.module.sys.pojo.FlowTask;
import com.system.handle.model.ResponseFrame;

public interface FlowTaskService {

	/**
	 * 完成任务
	 * @param taskId
	 * @param params
	 * @return
	 */
	public ResponseFrame pass(String taskId, Map<String, Object> params);
	
	/**
	 * 根据用户和流程key获取任务列表
	 * @param candidateUser
	 * @param processKey
	 * @return
	 */
	public List<FlowTask> findTask(String candidateUser, String processKey);
	
	/**
	 * 驳回任务
	 * @param taskId		当前任务ID
	 * @param params
	 * @return
	 */
	public ResponseFrame back(String taskId, Map<String, Object> params);
	
	/**
	 * 根据taskId获取任务对象
	 * @param taskId
	 * @return
	 */
	public Task getByTaskId(String taskId);
	/**
	 * 根据流程实例编号获取任务列表
	 * @param processInstanceId
	 * @return
	 */
	public List<Task> findByProcessInstanceId(String processInstanceId);

	public int getCountByProcessInstanceId(String processInstanceId);

	public Task getByProcessInstanceId(String processInstanceId);
}