package com.flow.module.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.flow.module.sys.pojo.FlowUser;

/**
 * flow_user的Dao
 * @author autoCode
 * @date 2017-12-29 10:12:18
 * @version V1.0.0
 */
public interface FlowUserDao {

	public abstract void save(FlowUser flowUser);

	public abstract void update(FlowUser flowUser);

	public abstract void delete(@Param("id")String id);

	public abstract FlowUser get(@Param("id")String id);

	public abstract List<FlowUser> findFlowUser(FlowUser flowUser);
	
	public abstract int findFlowUserCount(FlowUser flowUser);

	public abstract FlowUser getBySysNoUserId(@Param("sysNo")String sysNo, @Param("userId")String userId);
}