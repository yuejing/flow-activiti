package com.ms.biz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.*")
/*@EnableAutoConfiguration(exclude={
		DataSourceAutoConfiguration.class,
		DataSourceTransactionManagerAutoConfiguration.class,
		HibernateJpaAutoConfiguration.class, //（如果使用Hibernate时，需要加）
})*/
@SpringBootApplication
public class FlowActivitiApplication {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(FlowActivitiApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(FlowActivitiApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner init(){
		return args -> {
			LOGGER.info("程序初始化...");
		};
	}
}
